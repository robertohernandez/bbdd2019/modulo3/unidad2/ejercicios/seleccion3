<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Seleccion 2';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de seleccion 2</h1>

        <p class="lead">Modulo 3 - unidad 2</p>
        
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.1</h3>
                        <p>Listar el numero de ciclistas que hay</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta1'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.2</h3>
                        <p>Listar numero de ciclistas del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta2'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                  </div>
                </div>
            
              
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.3</h3>
                        <p>Edad media de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta3'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.4</h3>
                        <p>Edad Media de los ciclistas del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta4'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         <!------------------------------------------->   
         
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.5</h3>
                        <p>Listar la edad media de los ciclistas por cada equipo</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta5'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
   
            
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.6</h3>
                        <p>Listar el numero de ciclistar por equipo</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta6'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.7</h3>
                        <p>Listar el numero total de puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta7'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.8</h3>
                        <p>Listar el numero total de puertos mayores de 1.500</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta8'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
             
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.9</h3>
                        <p>Listar nombre de los equipos con mas de 4 ciclistas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta9'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.10</h3>
                        <p>Listar el nombre de los equipos con mas de 4 ciclistas cuyda edad este entre 28 y 32</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta10'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1.11</h3>
                        <p>Numer de etapas que ha ganado cada ciclista</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Dao', ['site/consulta11'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
        

    </div>
</div>
