<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionCrud(){
        return $this->render("gestion");
    }   
     
    public function actionConsulta1(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand('SELECT count(*) FROM ciclista')
//                ->queryScalar();
//                
           $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(*)totalCiclistas FROM ciclista',
            //'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['totalCiclistas'],
            "titulo"=>"Consulta 1 con Dao",
            "enunciado"=>"Numero total de ciclistas",
            "sql"=>"SELECT COUNT(*)totalCiclistas FROM ciclista",
        ]);
    }
    
    
    public function actionConsulta1a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("count(*) dorsal"),
           
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Numero total de ciclistas",
            "sql"=>"SELECT COUNT(*)totalCiclistas FROM ciclista",
        ]);
    
      
    }
    public function actionConsulta2(){
        //mediante DAO         
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT COUNT(*) nomequipo FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 1.2 con Dao",
            "enunciado"=>"Numero de ciclistas del equipo Banesto",
            "sql"=>"SELECT COUNT(*) nomequipo FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("count(*) dorsal")->distinct()
                 ->where("nomequipo ='Banesto'"),
            
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1.2 con Active Record",
            "enunciado"=>"Numero de ciclistas del equipo Banesto",
            "sql"=>"SELECT COUNT(*) nomequipo FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    
    public function actionConsulta3(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad)edad FROM ciclista",
//            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.3 con Dao",
            "enunciado"=>"Edad Media de los ciclistas",
            "sql"=>"SELECT AVG(edad) FROM ciclista",
        ]);
    }
    
    
    
    public function actionConsulta3a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("Avg(edad) edad")->distinct(),
              
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>" SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' or nomequipo = 'Amore Vita'",
        ]);
    
      
    }
    
    
     public function actionConsulta4(){
        //mediante DAO
//        $numero = yii::$app->db
//                ->createCommand("SELECT count(distinct edad) From ciclista WHERE nomequipo ='Artiach' or nomequipo ='Amore Vita'")
//                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT AVG(edad)edad FROM ciclista where nomequipo = 'banesto'",
//            'totalCount' => $numero,
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1.4 con Dao",
            "enunciado"=>"Edad Media de los ciclistas del equipo Banesto",
            "sql"=>"SELECT AVG(edad)edad FROM ciclista where nomequipo = 'banesto'",
        ]);
    }
    
    
    
    public function actionConsulta4a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("Avg(edad) edadMedia")
                 ->where("nomequipo='Banesto'"),
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 1.4 con Active Record",
            "enunciado"=>"Edad media de los ciclistas del equipo Banesto",
            "sql"=>"SELECT Avg(edad)edad FROM ciclista WHERE nomequipo = 'Banesto'"
        ]);
    
      
    }
        public function actionConsulta5(){
        
         //mediante DAO
        $numero = yii::$app->db
                ->createCommand("SELECT avg(edad)edad_media from ciclista group by nomequipo")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nomequipo, avg(edad)edad_media from ciclista group by nomequipo",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edad_media'],
            "titulo"=>"Consulta 1.5 con Dao",
            "enunciado"=>"Edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, avg(edad) edad_media from ciclista group by nomequipo",
            
        ]);
    
    }
    
    public function actionConsulta5a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("nomequipo, Avg(edad) edadMedia")
                 ->groupBy("nomequipo"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edadMedia'],
            "titulo"=>"Consulta 1.5 con Active Record",
            "enunciado"=>"Listar Edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, avg(edad)edad_media from ciclista group by nomequipo",
        ]);
    
    }
     public function actionConsulta6(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT nomequipo,count(*) total From ciclista Group by nomequipo")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nomequipo,count(*) total From ciclista Group by nomequipo",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','total'],
            "titulo"=>"Consulta 1.6 con Dao",
            "enunciado"=>"Listar numero de ciclista por equipo",
            "sql"=>"SELECT nomequipo,count(*) total From ciclista Group by nomequipo",
        ]);
    
    }
    
    public function actionConsulta6a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
                 ->select("nomequipo, count(*) total")
                ->groupBy("nomequipo"),
                
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','total'],
            "titulo"=>"Consulta 1.6 con Active Record",
            "enunciado"=>"Listar el numero de ciclistar por equipo",
            "sql"=>"SELECT nomequipo,count(*) total From ciclista Group by nomequipo",
        ]);
    
    }
    public function actionConsulta7(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) totalPuertos FROM puerto")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT count(*) totalPuertos FROM puerto",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['totalPuertos'],
            "titulo"=>"Consulta 1.7 con Dao",
            "enunciado"=>"Listar el numero total de puertos",
            "sql"=>"SELECT count(*) totalPuertos FROM puerto",
            
        ]);
    
    }
    
    public function actionConsulta7a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
                 ->select("count(*) totalPuertos"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalPuertos'],
            "titulo"=>"Consulta 1.7 con Active Record",
            "enunciado"=>"Numero total de puertos",
            "sql"=>"SELECT count(*) totalPuertos FROM puerto",
        ]);
    
    }
    
    
     public function actionConsulta8(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT count(*) totalPuertos FROM puerto WHERE altura > 1500")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT count(*) totalPuertos FROM puerto WHERE altura > 1500",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['totalPuertos'],
            "titulo"=>"Consulta 1.8 con Dao",
            "enunciado"=>"Listar el total de puertos con altura superior a 1500",
            "sql"=>"SELECT count(*) totalPuertos FROM puerto WHERE altura > 1500",
            
        ]);
    
    }
    
    public function actionConsulta8a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Puerto::find()
              ->select('count(*) totalPuertos')
             ->where("altura > 1500"),
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['totalPuertos'],
            "titulo"=>"Consulta 1.8 con Active Record",
            "enunciado"=>"Total de puertos con altura superior a 1500",
            "sql"=>"SELECT count(*) totalPuertos FROM puerto WHERE altura > 1500",
        ]);
    
    }
    public function actionConsulta9(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 1.9 con Dao",
            "enunciado"=>"Equipos con más de 4 ciclistas en sus filas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
            
        ]);
    
    }
    
    public function actionConsulta9a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
              ->select('nomequipo')
             ->groupBy('nomequipo')
             ->having("count(*)>4"),    
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 1.9 con Active Record",
            "enunciado"=>"Equipos con mas de 4 ciclistas en sus filas",
            "sql"=>"SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    
    }
    
    public function actionConsulta10(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT c.nomequipo FROM ciclista c  WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT c.nomequipo FROM ciclista c  WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 1.9 con Dao",
            "enunciado"=>"Equipos con mas de 4 ciclistas en sus filas y cuya edad esta entre 28 y 32 años",
            "sql"=>"SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4",
            
        ]);
    
    }
    
    public function actionConsulta10a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Ciclista::find()
              ->select('nomequipo')
              ->where('edad between 28 and 32')   
             ->groupBy('nomequipo')
             ->having("count(*)>4"),    
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 1.9 con Active Record",
            "enunciado"=>"Equipos con mas de 4 ciclistas en sus filas y cuya edad esta entre 28 y 32 años",
            "sql"=>"SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4",
        ]);
    
    }
    public function actionConsulta11(){
        //mediante Dao
         
        $numero = yii::$app->db
                ->createCommand("SELECT dorsal, COUNT(*) FROM etapa GROUP BY dorsal")
                ->queryScalar();
                
           $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal, COUNT(*)etapas FROM etapa GROUP BY dorsal",
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' =>5,
            ],   
        ]);
        
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas'],
            "titulo"=>"Consulta 1.11 con Dao",
            "enunciado"=>"Numero de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) FROM etapa GROUP BY dorsal",
            
        ]);
    
    }
    
    public function actionConsulta11a(){
        //mediante Active Record
        
         $dataProvider = new ActiveDataProvider([
             'query' => Etapa::find()
              ->select('dorsal,count(*) etapas')
              ->groupBy("dorsal"),   
             'pagination'=>[
                 'pageSize'=>5,
             ]
         ]);

        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','etapas'],
            "titulo"=>"Consulta 1.11 con Active Record",
            "enunciado"=>"Numero de etapas que ha ganado cada ciclista",
            "sql"=>"SELECT dorsal, COUNT(*) FROM etapa GROUP BY dorsal",
        ]);
    
    }
    
    
}
